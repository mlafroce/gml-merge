#!/usr/bin/env python
# coding: utf-8

import re
from os import listdir
from os.path import join
from sys import argv

HEADER_SIZE = 2
GML_HEADER = "graph\n[ directed 0"
GML_FOOTER = "]"
NODE_LINE = "\tnode ["
EDGE_LINE = "\tedge ["
BLOCK_END = "\t]"
SOURCE_PATTERN = r"\t*source ([0-9]*)\n"
TARGET_PATTERN = r"\t*target ([0-9]*)\n"
LABEL_PATTERN = r'\t*label "([^"]*)"\n'
NODEID_PATTERN = r"\t*id ([0-9]*)\n"
GMLNODE_FORMAT = '\tnode [\n\t\tid {0}\n\t\tlabel "{1}"\n\t]'
GMLEDGE_FORMAT = '\tedge [\n\t\tsource {0}\n\t\ttarget {1}\n\t]'
FILE_PATTERN=r"([^\\\/]*)\.gml$"

class GmlFile:
	def __init__(self):
		self.clean()

	"""Parseo un arista"""
	def _addEdge(self, src):
		line = src.readline()
		""" regex para parsear el número del source """
		source = int(re.match(SOURCE_PATTERN, line).group(1))
		line = src.readline()
		""" regex para parsear el número del target """
		target = int(re.match(TARGET_PATTERN, line).group(1))
		edge = GmlEdge(source, target)
		self.edgeSet.add(edge)
		""" Linea en donde cierra el arista """
		line = src.readline()

	"""Paseo un nodo"""
	def _addNode(self, src):
		line = src.readline()
		""" regex para parsear el número del id """
		currentId = int(re.match(NODEID_PATTERN, line).group(1))
		line = src.readline()
		label = re.match(LABEL_PATTERN, line).group(1)
		""" TODO: leer los otros datos presentes """
		while (line.rstrip('\n') != BLOCK_END):
			line = src.readline()
		node = GmlNode(currentId, label)
		self.nodeSet.add(node)
		self.nodeIdSet.add(node.nodeId)

	def clean(self):
		self.edgeSet = set()
		self.nodeSet = set()
		"""Set de ids de nodos"""
		self.nodeIdSet = set()
		self.currentId = 0

	def load(self, fileName):
		with open(fileName) as src:
			"""Avanzo hasta el primer nodo"""
			for i in range(HEADER_SIZE):
				line = src.readline()
			"""Leo hasta que se termine el archivo"""
			while (line):
				line = src.readline().rstrip('\n')
				if (line ==  NODE_LINE):
					self._addNode(src)
				elif (line ==  EDGE_LINE):
					self._addEdge(src)

	""" Agrega al dueño del grafo, amigo de todos los nodos del grafo """
	def addOwner(self, ownerName):
		while self.currentId in self.nodeIdSet:
			self.currentId += 1
		""" CurrentId ahora es un id nuevo """
		node = GmlNode(self.currentId, ownerName)
		self.nodeSet.add(node)
		""" Armo conexiones con todos """
		for nodeId in self.nodeIdSet:
			if (nodeId != self.currentId):
				self.edgeSet.add(GmlEdge(nodeId, self.currentId))

class GmlEdge:
	def __init__(self, source, target):
		self.source = source
		self.target = target

	""" Este lo tengo solo con fines de debug """
	def __str__(self):
		return "Edge => Source: {0} - Target: {1}".format(self.source, self.target)

	def toString(self):
		return GMLEDGE_FORMAT.format(self.source, self.target)

class GmlNode:
	def __init__(self, nodeId, label):
		self.nodeId = nodeId
		self.label = label

	""" Este lo tengo solo con fines de debug """
	def __str__(self):
		return "Node => NodeId: {0} - Target: {1}".format(self.nodeId, self.label)

	def toString(self):
		return GMLNODE_FORMAT.format(self.nodeId, self.label)

class GmlPool:
	def __init__(self):
		self.currentId = 0
		self.labelIdMap = {}
		self.edgeSet = set()
		self.nodeSet = set()

	def addFile(self, gmlFile):
		""" Key: id local, value: id global """
		nodeIdMap = {}
		""" Busca personas nnuevas """
		for node in gmlFile.nodeSet:
			if (node.label not in self.labelIdMap):
				self.labelIdMap[node.label] = self.currentId
				self.currentId += 1
			""" Mapeo el id local del archivo """
			nodeIdMap[node.nodeId] = self.labelIdMap[node.label]
		for edge in gmlFile.edgeSet:
			newSource = nodeIdMap[edge.source]
			newTarget = nodeIdMap[edge.target]
			self.edgeSet.add(GmlEdge(newSource, newTarget))

	def save(self, fileName):
		with open(fileName, 'w') as outFile:
			outFile.write(GML_HEADER + '\n')
			for label, nodeId in self.labelIdMap.iteritems():
				nodeStr = GmlNode(nodeId, label).toString() + '\n'
				outFile.write(nodeStr)
			for edge in self.edgeSet:
				outFile.write(edge.toString() + '\n')
			outFile.write(GML_FOOTER + '\n')
		
print "Inicia el programa"

dataDir = "test"
outFile = "test.gml"
if (len(argv) > 1):
	dataDir = argv[1]

if (len(argv) > 2):
	outFile = argv[2]

gmlPool = GmlPool()
for fileName in listdir(dataDir):
	filePath = join(dataDir, fileName)
	gmlName = re.match(FILE_PATTERN, fileName).group(1)
	gml = GmlFile()
	gml.load(filePath)
	gml.addOwner(gmlName)
	""" Ahora que el archivo tiene todas las conexiones, lo agrego al pool """
	gmlPool.addFile(gml)

gmlPool.save(outFile)

print "Termina el programa"

